import org.junit.*;

import java.util.Date;

import static org.junit.Assert.*;

public class hotelDateGeneratorTest {
    HotwireUtil hotwireUtil;
    long nowTime;

    @Before
    public void init() {
        hotwireUtil = new HotwireUtil();
        nowTime = new Date().getTime();
    }

    @Test(expected = IllegalArgumentException.class)
    public void genStayTestWithTooBigStartOffset() {
        hotwireUtil.genStay(new DaysCount(344), new DaysCount(16));
    }

    @Test(expected = IllegalArgumentException.class)
    public void genStayTestWithTooBigMaxStay() {
        hotwireUtil.genStay(new DaysCount(15), new DaysCount(354));
    }

    @Test
    public void periodEndBiggerThanPeriodStart() {
        Period period = hotwireUtil.genStay();
        assertTrue(period.end.getTime() > period.start.getTime());
    }

    @Test
    public void generatedPeriodMustBeBetweenStartOffsetAndMaxStay() {
        DaysCount startOffset = new DaysCount(10);
        DaysCount maxStay = new DaysCount(30);
        long startTime = nowTime + startOffset.toMillis();
        long maxEndTime = startTime + maxStay.toMillis();
        Period period = hotwireUtil.genStay(startOffset, maxStay);
        assertTrue(period.start.getTime() >= startTime);
        assertTrue(period.end.getTime() <= maxEndTime);
    }
}
