package awr

import java.io.File
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import Util.StringUtil
import scala.io.Source

class AwrDataTest extends FunSuite with BeforeAndAfter {

  import File.{separator => separ}

  val checkUrlStr = s"src${separ}test${separ}res${separ}jenkins-GatlingAllVerticals-172.html"
  val currentUrlStr = s"src${separ}test${separ}res${separ}jenkins-GatlingAllVerticals-177.html"
  var awrData: AwrComparison = _
  var outputResult: String = _

  def getFileContentStr(pathToFileStr: String) = Source.fromFile(pathToFileStr).getLines().mkString

  before {
    awrData = new AwrComparison(getFileContentStr(checkUrlStr), getFileContentStr(currentUrlStr))
    outputResult = new AwrComparisonRenderer(awrData.topComparison, checkUrlStr, currentUrlStr).fullHtml.toString()
    outputResult.saveToFile(new File(s"src${separ}test${separ}tmp${separ}awrComparisonTest.html"))
  }

  test("output result has <table tag") {
    assert(outputResult.contains("<table"))
  }

  test("output result has <html tag") {
    assert(outputResult.contains("<html"))
  }

  test("output result has <head> tag") {
    assert(outputResult.contains("<head>"))
  }

  test("output result has <body> tag") {
    assert(outputResult.contains("<body>"))
  }

}
