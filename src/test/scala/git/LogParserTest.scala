package git

import java.io.{FileNotFoundException, File}

import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter

class LogParserTest extends FunSuite with BeforeAndAfter {

  import File.{ separator => separ }
  val testResBasePathStr = s"src${separ}test${separ}res"
  val testLogFilePathStr = s"$testResBasePathStr${separ}log11.log"
  var logParser: LogParser = _

  before {
    logParser = new LogParser(testLogFilePathStr)
  }

  test("Check if commits extracted and exist") {

    var commits: Array[Commit] = Array()

    assert(
      logParser.last100Commits match {
        case Left(e) => println(e.getMessage); false
        case Right(foundCommits) => commits = foundCommits; true
      }
    )

    assert(commits.length > 0)

  }

  test("Check 'log not found' case") {
    intercept[FileNotFoundException] {
      new LogParser("non_existing_path.none")
    }
  }

}
