package git

import org.eclipse.jgit.lib.Constants
import org.joda.time.{DateTime, Interval}
import org.scalatest.{FunSuite, BeforeAndAfter}

abstract class AbstractLogExtractorTest extends FunSuite with BeforeAndAfter {

  val repoPathStr: String
  val branch: String

  lazy val logExtractor = new LogExtractor(repoPathStr, branch)

  val interval = new Interval(new DateTime().minusDays(7), new DateTime)

  var commits: Iterable[String] = _

  before {
    commits = {
      for (item <- logExtractor.getLog(interval).get) yield
      List(
        item.getAuthorIdent.getWhen,
        item.getAuthorIdent.getName,
        item.getFullMessage.init,
        item.getId.getName.take(7)
      ).mkString("", " ", "\n")
    }

    println(commits)
  }

  test("Check if commits extracted and exist for last 7 days") {
    assert(commits.nonEmpty)
  }

}

class VladLocalLogExtractorTest extends AbstractLogExtractorTest {
  val repoPathStr = "F:\\git\\zeromadness"
  val branch = "origin/jgitTest"
}

class AlexLocalLogExtractorTest extends AbstractLogExtractorTest {

  val repoPathStr = "C:\\Users\\obfurman\\IdeaProjects\\zeromadness33"
  val branch = "origin/jgitTest"

}

class VladRemoteLogExtractorTest extends AbstractLogExtractorTest {

  val repoPathStr = "C:\\work\\git2\\gatling2"
  val branch = "origin/develop"

}