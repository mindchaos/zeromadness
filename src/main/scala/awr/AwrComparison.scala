package awr

import scala.util.matching.Regex
import scala.xml.Node
import Util._

object AwrComparison {

  type AwrTable = Node
  type AwrTableRow = Node

  case class AwrComparisonTable(tableName: String, tableBody: Option[AwrComparisonTableBody])
  case class AwrComparisonTableBody(headerRow: Node, data: Seq[AwrComparisonTableRow])
  case class AwrComparisonTableRow(id: String, awrRow1: Option[AwrTableRow], awrRow2: Option[AwrTableRow])

  abstract class JoinField(val findRegEx: Regex)
  case object SqlId extends JoinField( """\s*SQL\s*Id\s*""".r)

  case class TableSearchCriteria(summary: String)
  val searchCriteria = List(
    "This table displays top SQL by elapsed time",
    "This table displays top SQL by CPU time",
    "This table displays top SQL by user I/O time",
    "This table displays top SQL by buffer gets",
    "This table displays top SQL by physical reads",
    "This table displays top SQL by unoptimized read requests",
    "This table displays top SQL by number of executions",
    "This table displays top SQL by number of parse calls",
    "This table displays top SQL by amount of shared memory used",
    "This table displays top SQL by version counts"
  ).map(TableSearchCriteria)
}

class AwrComparison(private val checkHtmlStr: String, private val currHtmlStr: String) {

  import AwrComparison._
  import scala.xml.XML

  private val (checkHtml, currHtml) = (XML.loadString(checkHtmlStr), XML.loadString(currHtmlStr))
  private val (checkTablesHtml, currentTablesHtml) = (checkHtml \\ "table", currHtml \\ "table")

  private def logWarning(message: String) = println(s"[WARNING]: $message")

  val topComparison: List[AwrComparisonTable] =
    searchCriteria.map(
      searchCriterion => {
        val checkTableEither :: currTableEither :: Nil =
          Seq(checkTablesHtml, currentTablesHtml).map(_.findBySummary(searchCriterion.summary)).toList

        (checkTableEither, currTableEither) match {
          case (Right(check), Right(curr)) => check.fullOuterJoin(curr, SqlId)
          case (Left(error), Right(curr)) => logWarning(error); curr.toAwrComparisonTable(SqlId)
          case (Right(check), Left(error)) => logWarning(error); check.toAwrComparisonTable(SqlId)
          case reason => logWarning(s"Can't find info from comparison tables: $reason"); AwrComparisonTable(searchCriterion.summary, None)
        }
      }
    )

}

