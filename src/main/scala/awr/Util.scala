package awr

import java.io.{File, FileOutputStream, PrintWriter}
import java.nio.file.{Paths, Path}
import awr.AwrComparison._
import scala.xml._
import scala.collection.JavaConverters._

object Util {

  def loadXML(url: String) = {
    val parserFactory = new org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl
    val parser = parserFactory.newSAXParser()
    val source = new org.xml.sax.InputSource(url)
    val adapter = new scala.xml.parsing.NoBindingFactoryAdapter

    adapter.loadXML(source, parser)
  }

  implicit class StringUtil(s: String) {
    def saveToFile(file: File) =
      new File(file.toPath.asScala.init.mkString).mkdirs() match {
        case true => Right {
          val writer = new PrintWriter(new FileOutputStream(file, false))
          writer.write(s)
          writer.close()
        }
        case false => Left(
          new RuntimeException(s"Can't create folder tree for saving file: ${file.getAbsolutePath}")
        )
      }
  }

  implicit class HtmlTablesUtil(tables: NodeSeq) {

    def findBySummary(summaryText: String): Either[String, Node] =
      tables.find(t => (t \ "@summary").text.contains(summaryText)) match {
        case None => Left(s"there is no table with: '$summaryText' summary")
        case Some(node) => Right(node)
      }

  }

  implicit class AwrTableUtil(table1: AwrTable) {

    lazy val tableHeader: NodeSeq = table1 \ "tr" \\ "th"
    lazy val tableSummary = (table1 \ "@summary").text

    lazy val table1Rows = (table1 \\ "tr").tail.map(_ \\ "td")

    def getNumIdColumn(onField: JoinField) = tableHeader.zipWithIndex.filter(_._1.text.matches(onField.findRegEx.toString())).head._2

    def getTableHeaderId(onField: JoinField) = tableHeader(getNumIdColumn(onField))

    def getTableHeaderNoId(onField: JoinField) = tableHeader.filterNot(th => onField.findRegEx.findFirstIn(th.text).isDefined)

    def getHeaderRow(onField: JoinField) = <tr>
      {Seq(getTableHeaderId(onField)) ++ getTableHeaderNoId(onField) ++ getTableHeaderNoId(onField)}
    </tr>

    def toAwrComparisonTable(onField: JoinField) = {
      implicit class RowsUtil(rows: Seq[NodeSeq]) {
        def filterId = rows.zipWithIndex.filterNot(_._2 == getNumIdColumn(onField)).map(_._1)
      }

      AwrComparisonTable(
        tableName = tableSummary,
        Some(
          AwrComparisonTableBody(
            getHeaderRow(onField),
            data = table1Rows.map(
              awrRow => AwrComparisonTableRow(
                id = awrRow(getNumIdColumn(onField)).text,
                Some(<tr>
                  {awrRow.filterId}
                </tr>),
                None
              )
            )
          )
        )
      )
    }

    def fullOuterJoin(table2: AwrTable, onField: JoinField) = {

      val numIdColumn = getNumIdColumn(onField)
      val table2Rows = (table2 \\ "tr").tail.map(_ \\ "td")

      val allKeys =
        (table1Rows.map(_(numIdColumn)) ++ table2Rows.map(_(numIdColumn)))
          .map(idNode => (idNode \ "a").text)
          .distinct

      implicit class RowsUtil(rows: Seq[NodeSeq]) {
        def findRowByKey(key: String): Option[NodeSeq] = rows.find(row => (row(numIdColumn) \ "a").text == key)

        def filterId = rows.zipWithIndex.filterNot(_._2 == numIdColumn).map(_._1)
      }

      implicit class NodeSeqOptUtil(node: Option[NodeSeq]) {
        def toTrNoIdOpt = node match {
          case Some(rowTds) => Some(<tr>
            {rowTds.filterId}
          </tr>)
          case _ => None
        }
      }

      AwrComparisonTable(
        tableName = tableSummary,
        Some(
          AwrComparisonTableBody(
            getHeaderRow(onField),
            data = allKeys.map(
              key => AwrComparisonTableRow(
                id = key,
                table1Rows.findRowByKey(key).toTrNoIdOpt,
                table2Rows.findRowByKey(key).toTrNoIdOpt
              )
            )
          )
        )
      )

    }

  }

}
