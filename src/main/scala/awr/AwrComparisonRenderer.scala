package awr

import AwrComparison.AwrComparisonTable

class AwrComparisonRenderer(tables: List[AwrComparisonTable], checkUrl: String, currUrl: String) {

  private def genTable(table: AwrComparisonTable) = {
    <div>
      <h4>
        {table.tableName}
      </h4>{table.tableBody match {
      case None => "Neither checkpoint nor current table not found"
      case Some(body) =>
        val header = body.headerRow \\ "th"
        val subTableHeaderSizeStr = ((header.size - 1) / 2).toString
        val emptyStyle = "text-align: center; font-size: 9pt; font-weight: bold"
        val emptyTag = (text: String) => <td colspan={subTableHeaderSizeStr} style={emptyStyle}>
          {text}
        </td>
        val goneTd = emptyTag("Gone")
        val newTd = emptyTag("New")

        <table cellpadding="1" cellspacing="2" style="border-style: solid; border-width: thin medium; border-color: Gray; background-color: white" class="AwrComparisonTable">
          <tr>
            <th rowspan="2">
              {header.head.text}
            </th>
            <th colspan={subTableHeaderSizeStr}>Checkpoint</th>
            <th colspan={subTableHeaderSizeStr}>Current</th>
          </tr>
          <tr>
            {header.tail}
          </tr>{(for (row <- body.data.filter(comparisonRow => comparisonRow.awrRow1.isEmpty || comparisonRow.awrRow2.isEmpty))
          yield {
            <tr>
              {(row.awrRow1, row.awrRow2) match {
              case (None, None) =>
                <td class="AwrComparisonTableTdId">
                  {row.id}
                </td>
                  <td colspan={(header.size - 1).toString}>No data associated with the key! This case shouldn't exist.</td>
              case (Some(check), None) =>
                <td class="AwrComparisonTableTdId">
                  <a href={checkUrl + "#" + row.id}>
                    {row.id}
                  </a>
                </td> +: (check \\ "td") :+ goneTd
              case (None, Some(curr)) =>
                <td class="AwrComparisonTableTdId">
                  <a href={currUrl + "#" + row.id}>
                    {row.id}
                  </a>
                </td> +: (newTd +: (curr \\ "td"))
              case (Some(check), Some(curr)) =>
                <td class="AwrComparisonTableTdId">
                  <a href={currUrl + "#" + row.id}>
                    {row.id}
                  </a>
                </td> +: ((check \\ "td") ++ (curr \\ "td"))
            }}
            </tr>
          }).sortBy(tr => (tr \\ "td")(1).text).reverse}
        </table>
            <br/>
            <br/>
    }}
    </div>
  }

  val tablesHtml = for (table <- tables) yield genTable(table)

  val fullHtml =
    <html>
      <head>
        <title>Awr Comparison</title>
      </head>
      <body>{tablesHtml}</body>
    </html>
}

