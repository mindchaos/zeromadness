package git

import org.joda.time.format.DateTimeFormat
import org.joda.time.DateTime
import scala.io.Source
import scala.util.control.Exception._

/**
 * The LogParser program implements an application that parsing git log file to Array[Commit]
 *
 * Log file must be generated with following format:
 *  %aN[splitter]%aE[splitter]%H[splitter]%ad[splitter]%at[splitter]%s[commitSplitter]
 *  e.g.:
 *  "git log --pretty=format:'%aNspl1tt3r%aEspl1tt3r%Hspl1tt3r%adspl1tt3r%atspl1tt3r%sl1n3split' --max-count=100 > log.log"
 */
case class Commit(author: String, authorDate: DateTime, commitNotes: String, hash: String, authorEMail: String, authorTimestamp: String)
class LogParser(logPathStr: String, commitSplitter: String = "l1n3split\n", fieldSplitter: String = "spl1tt3r") {

  private val defaultGitDateFormat: String = "EEE MMM dd HH:mm:ss yyyy Z"

  private val source = Source.fromFile(logPathStr)
  private val commitsSourceEither =
    allCatch withApply { e => Left(new RuntimeException(e.getMessage)) } andFinally { source.close() } apply { Right(source.mkString) }
  private val formatter = DateTimeFormat.forPattern(defaultGitDateFormat)

  val last100Commits: Either[RuntimeException, Array[Commit]] = commitsSourceEither match {
    case Right(commitsSource) => Right(
      for (commitSource <- commitsSource.split(commitSplitter)) yield
      {
        val fields = commitSource.split(fieldSplitter)
        Commit(fields(0), new DateTime(formatter.parseDateTime(fields(3))), fields(5), fields(2), fields(1), fields(4))
      }
    )
    case Left(e) => Left(e)
  }

}