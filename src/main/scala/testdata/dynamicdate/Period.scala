package testdata.dynamicdate

import java.text.SimpleDateFormat
import java.util.Date

object Period {
  val formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
  implicit class StringFormat(date: Date) {
    def readable = formatter.format(date)
  }
}

case class Period(start: Date, end: Date) {

  import Period.StringFormat

  validatePositive()

  private def validatePositive() =
    if (end.getTime < start.getTime)
      throw new IllegalArgumentException("End time is earlier than start time")

  override def toString = {
    s"testdata.dynamicdate.Period(\n\tStart: ${start.readable} \n\tEnd:   ${end.readable}\n)"
  }

}