package testdata.dynamicdate

import java.util.Calendar

import testdata.dynamicdate.CustomTypes.DaysCount

import scala.util.Random

object HotwireUtil {

  private val randomGenerator = new Random
  private val startOffsetLimit = 329
  private val maxStayLimit = 30

  //TODO: randomize start date
  def genStay(startOffset: DaysCount = 10, maxStay: DaysCount = 30) = {

    validateStartOffset(startOffset)
    validateMaxStay(maxStay)

    val (start, end) = (Calendar.getInstance, Calendar.getInstance)
    start.add( Calendar.DATE, startOffset )
    end.add( Calendar.DATE, startOffset + 1 + randomGenerator.nextInt(maxStay) )

    Period(start.getTime, end.getTime)

  }

  private def validateStartOffset(startOffset: DaysCount) =
    if (startOffset > startOffsetLimit)
      throw new IllegalArgumentException("Start offset can't be longer than 329 days")

  private def validateMaxStay(maxStay: DaysCount) =
    if (maxStay > maxStayLimit)
      throw new IllegalArgumentException("Max stay can't exceed ")

}
