package testdata.dynamicdate

object Main {

  def main(args: Array[String]) {
      printMany(100)
  }

  def printMany(count: Int): Unit =
    count match {
      case done if done == 0 => ;
      case next =>
        println( s"Date period #$count:\n${HotwireUtil.genStay()}\n" )
        printMany(count-1)
    }

}
