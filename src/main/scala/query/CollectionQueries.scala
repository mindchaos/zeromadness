package query

/**
 * Act with collections in SQL like manner. Created by Vlad on 14.07.2015.
 */

import TableTypes._
import TableExamples._

object CollectionQueries {

  def main(args: Array[String]) {

//    println( table1.fullOuterJoin(table2,0) )

  }

  trait JoinType
  case object InnerJoin extends JoinType
  case object LeftJoin extends JoinType
  case object RightJoin extends JoinType
  case object FullOuterJoin extends JoinType

  implicit class JoinUtil[T1 <: Seq[Any], T2 <: Seq[T1]](table1: T2) {

    def join(table2: T2, idColumnNum: Int, joinType: JoinType): Map[Any, (T1, T1)] = {

      val all = for ( row1 <- table1; row2 <- table2 ) yield (row1, row2)

      val results = joinType match {
        case InnerJoin => all
          .filter( row => row._1(idColumnNum) == row._2(idColumnNum) )
          .map( row => row._1(idColumnNum) -> row )

          //TODO implement other join types
      }

      results.toMap

    }

    def innerJoin(table: T2, idColumnNum: Int) = join(table, idColumnNum, InnerJoin)
    def leftJoin(table: T2, idColumnNum: Int) = join(table, idColumnNum, LeftJoin)
    def rightJoin(table: T2, idColumnNum: Int) = join(table, idColumnNum, RightJoin)
    def fullOuterJoin(table: T2, idColumnNum: Int) = join(table, idColumnNum, FullOuterJoin)

  }

}
