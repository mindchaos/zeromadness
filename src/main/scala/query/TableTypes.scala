package query

/**
 * Types representing table. Created by Vlad on 15.07.2015.
 */
object TableTypes {

  type Column = Any
  type Row = Array[Column]
  type HeaderRow = Row
  type Table = Array[Row]

  def Row(xs: Column*) = xs.toArray
  def HeaderRow(xs: Column*) = Row(xs)
  def Table(rows: Row*) = rows.toArray[Row]

}
