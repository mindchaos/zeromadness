package query

import query.TableTypes._

object TableExamples {

  val headerRow = HeaderRow( "id", "field1", "field2" )

  val table1 = Table(
    headerRow,
    Row("1", "val11", "val12"),
    Row("2", "val21", "val22")
  )

  val table2 = Table(
    headerRow,
    Row("1", "val31", "val32"),
    Row("3", "val41", "val42")
  )

}
