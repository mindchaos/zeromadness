import java.util.Date;
import java.util.Random;

public class HotwireUtil {
    public static Period genStay() {
        return genStay(new DaysCount(10), new DaysCount(30));
    }

    public static Period genStay(DaysCount startOffset, DaysCount maxStay) {
        validateStartOffset(startOffset);
        validateMaxStay(startOffset, maxStay);
        Random rng = new Random();
        long nowTime = new Date().getTime();
        long startTime = nowTime + startOffset.toMillis();
        long endTime = startTime + DaysCount.toMillis(1 + rng.nextInt(maxStay.toInt()));
        return new Period(new Date(startTime), new Date(endTime));
    }

    private static void validateStartOffset(DaysCount startOffset) {
        if (startOffset.toInt() > 329) {
            throw new IllegalArgumentException("startOffset can't be longer than 329 days");
        }
    }

    private static void validateMaxStay(DaysCount startOffset, DaysCount maxStay) {
        if (maxStay.toInt() > (329 - startOffset.toInt())) {
            throw new IllegalArgumentException("startOffset can't be that long");
        }
    }
}