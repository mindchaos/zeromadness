
public class DaysCount {
    public DaysCount(int count) {
        value = count;
    }

    private int value;

    public int toInt() {
        return value;
    }

    public long toMillis() {
        return value * 24L * 60 * 60 * 1000;
    }

    public static long toMillis(int daysCount) {
        return daysCount * 24L * 60 * 60 * 1000;
    }
}
