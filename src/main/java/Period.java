import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Period {
    Date start;
    Date end;

    public Period(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    public String toString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return "Start: " + dateFormat.format(start) + "\nEnd: " + dateFormat.format(end);
    }

}